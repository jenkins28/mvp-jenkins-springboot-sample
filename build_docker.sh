# docker login 필요함 
timestamp=$(date +%s)

tag=0.0.1
tag=${tag}-${timestamp}
image=happycloudpak/springboot-service
baseDir=.
baseDeployDir=/deployment-iks

echo tag=${tag}
echo image=${image}


docker build -f ${baseDir}${baseDeployDir}/Dockerfile -t ${image}:${tag} ${baseDir}
docker push ${image}:${tag}
docker tag ${image}:${tag} ${image}:latest
docker push ${image}:latest
